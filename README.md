# QualityFeedback

For Software Project course in UI

To run the project you have to:

1. Download docker-compose.yml
2. Run the following command in terminal:
        
        $ docker-compose up

3. Run these two commands in diferent terminal, but the same folder, or add to previous flag " -d" ath the end:
        
        $ docker-compose exec web python manage.py migrate
        $ docker-compose exec web python manage.py createsuperuser
4. The setup is ready, but to use it properly you would have to go localhost:8000/admin and add user and profile for him manually. (One of requirements were so that only administrators could add users)