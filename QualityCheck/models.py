import datetime

from django.db import models
from django.contrib.auth.models import User
from django.conf import settings


class Template(models.Model):
    name = models.CharField(max_length=50)
    data = models.CharField(max_length=1024)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    secondname = models.CharField(max_length=50)
    status = models.CharField(max_length=50)
    identification_code = models.CharField(max_length=50, default='null')
    telegram_id = models.CharField(max_length=20, default='null')

    def add(self):
        self.save()


class Course(models.Model):
    name = models.CharField(max_length=50)
    audience = models.CharField(max_length=50)
    template = models.ForeignKey(Template, default=-1, on_delete=models.SET_DEFAULT)
    professor = models.ForeignKey(settings.AUTH_USER_MODEL, default=-1, on_delete=models.SET_DEFAULT)

    def add(self):
        self.save()


class Form(models.Model):
    data = models.CharField(max_length=2000)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, on_delete=models.CASCADE)
    course = models.ForeignKey(Course, default=1, on_delete=models.CASCADE)
    type = models.CharField(max_length=20, default='lecture')
    number = models.IntegerField(default=-1)


class Schedule(models.Model):
    start = models.CharField(max_length=20)
    end = models.CharField(max_length=20)
    day = models.CharField(max_length=20)
    course = models.ForeignKey(Course, default=1, on_delete=models.CASCADE)
    type = models.CharField(max_length=20)
