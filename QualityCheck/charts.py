from jchart import Chart

class PriceChart(Chart):
    chart_type = 'line'
    data = []

    def get_datasets(self, currency_type):
        data = [{'x': price.date, 'y': price.point} for price in prices]
        return [DataSet(data=data)]