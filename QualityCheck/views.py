from django.http import JsonResponse
from django.shortcuts import render, redirect, render_to_response
from django.views import View
import django.contrib.auth as auth
import json
from django.template.context_processors import csrf
from django.contrib.auth.forms import AuthenticationForm
from django.views.decorators.csrf import csrf_exempt
from .models import *
from django.contrib.auth.decorators import login_required
from .forms import *
from django.contrib.auth import logout
from collections import Counter
import numpy
import random
from django.utils.crypto import get_random_string
from django.core.mail import send_mail

# URL where bot is running
BOT_URL = ''


# verified
class LoginForm(View):
    def get(self, request):
        if auth.get_user(request).is_authenticated:
            return redirect('/courses')
        else:
            context = self.create_context_username_csrf(request)
            return render_to_response('login.html', context=context)

    def post(self, request):
        request.POST = request.POST.copy()
        request.POST['username'] = request.POST['username'].lower()
        form = AuthenticationForm(request, data=request.POST)

        if form.is_valid():
            auth.login(request, form.get_user())
            return redirect('/courses')
        else:
            return render(request, 'login.html')

    def create_context_username_csrf(self, request):
        context = {}
        context.update(csrf(request))
        context['login_form'] = AuthenticationForm
        return context


# verified
@login_required
def courses(request):
    courses = Course.objects.filter(audience=request.user.profile.status)
    prof = Course.objects.filter(professor=request.user)

    return render(request, 'courses.html', {'courses': courses | prof})


# verified
@login_required
def add_member(request):
    """
          Provides Backbone for frontend adding of new member

          :param request: HttpRequest
                      Can be GET and can be POST. On POST method it accepts request with fields:
                          'email' - email of new user,
                          'first_name', 'second_name', 'last_name',
                          'status' - can have values 0-7: 0 - member of DoE, 1-4 - Bachelor year,
                                            5-6 - Master year, 7 - professor
          :return: On GET returns 'add_member.html' page
                  On POST returns the same page reseted on success and on failure.
          """
    if request.method == 'POST':
        if request.user.profile.status == '0':
            form = ProfileForm(request.POST)
            if form.is_valid():
                email = form.cleaned_data.get('email').lower()
                first_name = form.cleaned_data.get('first_name')
                last_name = form.cleaned_data.get('last_name')
                second_name = form.cleaned_data.get('second_name')
                status = form.cleaned_data.get('status')
                add_member_to_db(email, first_name, last_name, second_name, status)
                return render(request, 'add_member.html')
            else:
                return render(request, 'add_member.html')
        else:
            return render(request, 'add_member.html', {'courses': courses})
    else:
        return render(request, 'add_member.html', {'courses': courses})


def add_member_to_db(email, first_name, last_name, second_name, status):
    password = get_random_string(length=8)
    message = 'Your password for our service is : ' + password + "\nPlease change it as soon as possible"
    send_mail('New Account', message, 'from@example.com', [email])
    user = User.objects.create_user(username=email,
                                    email=email,
                                    password=password,
                                    first_name=first_name,
                                    last_name=last_name)
    user.save()
    user.refresh_from_db()
    Profile(user_id=user.id).save()
    user.profile.secondname = second_name
    user.profile.status = status
    user.profile.identification_code = random.randint(1000, 99999)
    user.profile.save()


# verified
@login_required
def add_course(request):
    """
        Provides Backbone for frontend adding of course

        :param request: HttpRequest
                    Can be GET and can be POST. On POST method it accepts request with fields:
                        'name' - name of the new course, 'audience' - what students will be taking this course,
                        'professor' - first and last name (with space in between this 2 words)
                                                            of professor for the new course
        :return: On GET returns 'add_course.html' page
                On POST returns the same page reseted on success and on failure.
        """
    if request.method == 'POST':
        form = CourseForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data.get('professor')
            name = form.cleaned_data.get('name')
            audience = form.cleaned_data.get('audience')
            add_course_to_db(email, name, audience)
            return render(request, 'add_course.html', {'courses': courses, 'professors':
                User.objects.filter(profile__status='7')})
        else:
            return render(request, 'add_course.html', {'courses': courses, 'professors':
                User.objects.filter(profile__status='7')})
        pass
    else:
        return render(request, 'add_course.html', {'courses': courses, 'professors':
            User.objects.filter(profile__status='7')})



def add_course_to_db(email, name, audience):

    user = User.objects.get(username=email)
    if user.profile.status == "7":
        course = Course.objects.create(professor_id=user.id, template_id=1)
        course.name = name
        course.audience = audience
        course.save()


# verified
@login_required
def course_details(request, id):
    course = Course.objects.get(id=id)
    if not course:
        return redirect('/courses')

    # Preparing statistics
    criteria = {}
    template = json.loads(course.template.data)['IntFields']
    for element in template:
        criteria[element] = {'min': 0, 'max': 0, 'val': []}

    for element in Form.objects.filter(course=course.id):
        fields = json.loads(element.data)['IntFields']
        for field in fields:
            criteria[field]['min'] = fields[field]['min']
            criteria[field]['max'] = fields[field]['max']
            criteria[field]['val'].append(fields[field]['val'])

    for cr in criteria:
        criteria[cr]['val'] = round(sum(criteria[cr]['val']) / len(criteria[cr]['val']), 1)

    weekly_stat = get_stat_for_type_body(course)

    return render(request, 'course_page.html', {'course': course, 'criteria': criteria, 'weekly_stat': weekly_stat})


@csrf_exempt
def add_form(request):
    """
    Accepts users answer to a poll
    :param request: HttpRequest
                Can be GET and can be POST. On POST method it accepts request with fields:
                    'telegram_id' - user's telegram id, 'data' - answer itself
    :return: On GET returns {'status': 0}
            On POST returns {'status': 1} on success, or {'status': 0} on failure
    """
    if request.method == 'POST':
        form = FormForm(request.POST)
        if form.is_valid():
            add_form_to_db(int(request.POST.get('telegram_id', -1)), form.cleaned_data.get('data'),
                           request.POST.get('course', -1))
            return JsonResponse({'status': 1})
        else:
            return JsonResponse({'status': 0})
        pass
    else:
        return JsonResponse({'status': 0})


def add_form_to_db(telegram_id, data, course):
    profile = Profile.objects.get(telegram_id=telegram_id)
    user = User.objects.get(id=profile.user_id)
    answer = Form.objects.create(user_id=user.id, course_id=course)
    answer.data = data
    answer.save()


@login_required
def add_template(request):
    """
            Provides Backbone for frontend of adding of new template

            :param request: HttpRequest
                        Can be GET and can be POST. On POST method it accepts request with fields:
                            'name' - name of the new template;
                            'data' - values of template, is a string that encapsulates
                                json file with fields 'CharFields' and 'IntFields";

            :return: On GET returns 'add_template.html' page
                    On POST returns the same page reseted on success and on failure.
            """

    if request.method == 'POST':
        form = TemplateForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data.get('name')
            data = form.cleaned_data.get('data')
            add_template_to_db(name, data)
            return render(request, 'add_template.html')
        else:
            return render(request, 'add_template.html')
        pass
    else:
        return render(request, 'add_template.html')


def add_template_to_db(name, data):
    template = Template.objects.create()
    template.name = name
    template.data = data
    template.save()


def see_users(request):
    users = User.objects.all()
    return render(request, 'users.html', {'users': users})


@csrf_exempt
def bind_tg_id(request):
    if request.method == 'POST':
        identification_code = request.POST.get('identification_code')
        telegram_id = request.POST.get('tg_id')
        print(telegram_id)
        profile = Profile.objects.get(identification_code=identification_code)

        data = {'success': 0, 'extra': ''}
        if profile:
            print(profile.telegram_id)
            if profile.telegram_id != '0':
                if profile.telegram_id == telegram_id:
                    data['extra'] = 'this user'
                else:
                    data['extra'] = 'another user'
            else:
                profile.telegram_id = telegram_id
                profile.save()
            data['success'] = 1
        return JsonResponse(data)


@csrf_exempt
def get_schedule_of_the_courses(request):
    """
        Sends all schedules for all courses.
        Example of request:

        > a = requests.get('http://127.0.0.1:8000/schedule/')
        > a.json()
        > {"39": [{"id": 3, "start": "16-32",
        "end": "12-23", "day": "tuesday", "course_id": 39, "type": "lab"}], "40": [{"id": 1, "start": "11-11",
        "end": "23-59", "day": "friday", "course_id": 40, "type": "lab"}, {"id": 2, "start": "11-45", "end": "23-44",
        "day": "monday", "course_id": 40, "type": "lecture"}]}

        :param request: {}
        :return: JsonResponse({ <course1.id> : [{<schedule1>},{<schedule2>},..], <course2.id>:[{schedule3}, ...], ...})

    """
    if request.method == 'GET':
        dict = {}
        courses = Course.objects.all()
        for course in courses:
            response = Schedule.objects.filter(course_id=course.id).values()
            dict[course.id] = {'data': list(response), 'name': course.name}
        return JsonResponse(dict)


# Returns template for course by course_id
@csrf_exempt
def send_template_to_bot(request):
    """
    Sends template for certain course.
    Example of request:

    > a = requests.post('http://127.0.0.1:8000/template_by_course/', data={'course': 39})
    > a.json()
    > {'data': '{"CharFields":{"What do you think about Succi?":"", "Else Blabla": " "},
    "IntFields":{"overall":{    "min" :0,"val":0, "max":10 }}}'}

    :param request: {'course': <any course id here>}
    :return: JsonResponse({'data' : {<template here>} )
    """
    if request.method == 'POST':
        form = SendForm(request.POST)
        if form.is_valid():
            course = Course.objects.get(id=form.cleaned_data.get('course'))
            return JsonResponse({'data': get_template(course).data})
        else:
            return JsonResponse({'data': {}})
        pass
    pass


def get_template(course):
    return Template.objects.get(id=course.template_id)


@login_required
def logout_view(request):
    logout(request)
    return redirect('/login')


@csrf_exempt
def assign_template(request):
    """
       Sends template for certain course.
       Example of request:

       > a = requests.post('http://127.0.0.1:8000/assign_template_to_course/', data={'template': 2, 'course': 40})
       > a.json()
       > {}

       :param request: {'course': <any course id here>, 'template': <id of template>}
       :return: {}
       """

    if request.method == 'POST':
        form = AssignTemplatForm(request.POST)
        if form.is_valid():
            course = form.cleaned_data.get('course')
            template = form.cleaned_data.get('template')
            assign_template_db(course, template)
            return redirect('/courses')
        else:
            return render(request, 'assign_template.html', {'error': 'Error occured'})
    else:
        courses = Course.objects.all()
        templates = Template.objects.all()
        return render(request, 'assign_template.html', {'courses': courses, 'templates': templates})


def assign_template_db(course_id, template):
    course = Course.objects.get(id=course_id)
    course.template_id = template
    course.save()


def get_answers(user):
    return Form.objects.select_related('course').filter(user_id=user)


def user(request, id):
    user = User.objects.get(id=id)
    forms = Form.objects.filter(user=user)

    return render(request, 'user.html', {'forms': forms})


def feedback(request, id):
    feedback = Form.objects.get(id=id)
    data = json.loads(feedback.data)
    return render(request, 'form.html', {'feedback': feedback, 'chars': data['CharFields']})


def get_template_list(request):
    """
    Accepts GET request and returns list of all templates
    :param request: GET
    :return: {'1': {'name': 'Main Template', 'data':
    {'CharFields': {'What do you think about Succi?': '', 'Else Blabla': ' '}, 'IntFields': {
    'overall': {'min': 0, 'val': 0, 'max': 10}}}},
    '2': {'name': 'Test template', 'data': {'CharFields': {'Your
    opinion about sprints?': '', 'Do you love sprints?': ' '}, 'IntFields': {'Total': {'min': 0, 'val': 0,
    'max': 10}}}}}

    """
    if request.method == "GET":
        templates = Template.objects.all()
        dict = {}
        for template in templates:
            dict[template.id] = {'name': template.name, 'data': json.loads(template.data)}
        return JsonResponse(dict)


@csrf_exempt
def get_adudience_for_course(request):
    """
        Gets audience's of certain course telegram ids.
        Example of request:

        > a = requests.post('http://127.0.0.1:8000/get_audience/', data={'course': 39})
        > a.json()
        > {"1": {"telegram_id": "56069837"}, "7": {"telegram_id": "0001"}}

        :param request: {'course': <any course id here>}
        :return: JsonResponse({<id of student> : {<telegram_id of user>}, <id of student> : {...}} )
        """
    if request.method == "POST":
        form = SendForm(request.POST)
        if form.is_valid():
            course = Course.objects.get(id=form.cleaned_data.get("course"))
            audience = Profile.objects.filter(status=course.audience)
            dict = {}
            for student in audience:
                dict[student.id] = {'telegram_id': student.telegram_id}
            print(dict)
            return JsonResponse(dict)
        else:
            return JsonResponse({'error': 'wrong input'})


@csrf_exempt
def for_pie_chart(request, course, type, number):
    """
        Gets distribution of answers values.
        Example of request:

        > a = requests.post('http://localhost:8000/pie/', data={'course': 39, 'type':'lecture', 'number': 1})
        > a.json()
        > {'overall': {'min': 0, 'max': 10, 'val': {'4': 2, '6': 1}}}

        :param request: {'course': <any course id here>, 'type':<can be lecture, lab, tutorial>,
                        'number': <number of class in the course>}
        :return: JsonResponse({<question 1>:{min:<min value for this field>, max:_ ,
                    'val': {<some mark>: <amount of people who've given this mark>}}, <question 2>:{...},...)
        """
    course = Course.objects.get(id=course)
    ty = '-1'
    if type == '0':
        ty = 'lecture'
    elif type == '1':
        ty = 'lab'
    elif type == '2':
        ty = 'tutorial'
    a = get_pie_body(course, ty, number)
    return JsonResponse(a)


def get_pie_body(course, type: str, number: str) -> dict:
    template = json.loads(course.template.data)['IntFields']
    criteria = {}
    for element in template:
        criteria[element] = {'min': 0, 'max': 0, 'val': []}
    forms = Form.objects.filter(course=course.id, type=type, number=number)
    for element in forms:
        fields = json.loads(element.data)['IntFields']
        for field in fields:
            criteria[field]['min'] = fields[field]['min']
            criteria[field]['max'] = fields[field]['max']
            criteria[field]['val'].append(fields[field]['val'])
    for cr in criteria:
        criteria[cr]['val'] = Counter(criteria[cr]['val'])
    return criteria


def get_feedback_stat_by_week(request, course):
    """
        Gets statistics for given course's classes of пшмут ензу.
        Example of request:

        > a = requests.post('http://localhost:8000/get_stat/', data={'course': 39, 'type': 'lab'})
        > a.json()
        > '{"1": {"overall": {"min": 0, "max": 10, "val": 5.0}}, "2": {"overall": {"min": 0, "max": 10, "val": 9.0}}}'

        :param request: {'course': <any course id here>, 'type':<can be lecture, lab, tutoeial>}
        :return: JsonResponse({<id of class> : {<question 1>:{<answer>}, <question 2>:{...},...},
                    <id of class> : {...}})
        """
    course = Course.objects.get(id=course)
    if type == '0':
        ty = 'lecture'
    if type == '1':
        ty = 'lab'
    if type == '2':
        ty = 'tutorial'
    dict = {}
    get_stat_for_type_body(course, ty, dict)

    return JsonResponse(dict)


def get_stat_for_type_body(course):
    dict = {}
    for i in range(1, 30):
        template = json.loads(course.template.data)['IntFields']
        criteria = {}
        for element in template:
            criteria[element] = {'min': 0, 'max': 0, 'val': []}
        forms = Form.objects.filter(course=course.id, number=i)
        if len(forms) != 0:
            for element in forms:
                fields = json.loads(element.data)['IntFields']
                for field in fields:
                    criteria[field]['min'] = fields[field]['min']
                    criteria[field]['max'] = fields[field]['max']
                    criteria[field]['val'].append(fields[field]['val'])
            for cr in criteria:
                criteria[cr]['val'] = numpy.average(criteria[cr]['val'])
            dict[element.number] = criteria
        else:
            break
    return dict


def statistics(request):
    return render(request, 'statistics.html')
