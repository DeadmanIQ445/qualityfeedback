"""QualityCheck URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf†
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from . import views
from django.views.generic.base import RedirectView
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('change-password/', auth_views.PasswordChangeView.as_view()),
    path('change-password/done/', auth_views.PasswordChangeDoneView.as_view(), name='password_change_done'),
    path('password-reset/', auth_views.PasswordResetView.as_view()),
    path('password-reset/done/', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    url('^$', RedirectView.as_view(url='login/')),

    url(r'^logout/$', views.logout_view),
    url(r'^login/$', views.LoginForm.as_view(), name='login'),
    url(r'^add_member/$', views.add_member, name='add_member'),
    url(r'^courses/$', views.courses),
    url(r'^course/(?P<id>[0-9]+)/$', views.course_details, name='course_details'),
    url(r'^add_course/$', views.add_course, name='add_course'),
    url(r'^add_template/$', views.add_template, name='add_template'),
    url(r'^add_form/$', views.add_form, name='add_form'),
    url(r'^bind/$', views.bind_tg_id, name='bind_tg_id'),
    url(r'^users/$', views.see_users, name='users'),
    url(r'^user/(?P<id>[0-9]+)/$', views.user, name='user'),
    url(r'^feedback/(?P<id>[0-9]+)/$', views.feedback, name='feedback'),
    url(r'^schedule/$', views.get_schedule_of_the_courses, name='schedule'),
    url(r'^templates/$', views.get_template_list, name='templates'),
    url(r'^template_by_course/$', views.send_template_to_bot, name='template_by_course'),
    url(r'^assign_template_to_course/$', views.assign_template, name='assign_template'),
    url(r'^get_audience/$', views.get_adudience_for_course, name='get_audience'),
    url(r'^pie/(?P<course>[0-9]+)/(?P<type>[0-9]+)/(?P<number>[0-9]+)/$', views.for_pie_chart),
    url(r'^get_stat/(?P<course>[0-9]+)/(?P<type>[0-9]+)/$', views.get_feedback_stat_by_week),
    url(r'^statistics/$', views.statistics),
]
