
from django.test import TestCase
from .. import models
from django.contrib.auth.models import User

class ProfileTestCase(TestCase):
    def setUp(self):
        user = User.objects.create(username="Test", first_name="Test", last_name="LTest", email="test@test.test", password="0000")
        user.save()
        user.refresh_from_db()
        models.Profile(user_id=user.id, identification_code='0', telegram_id='0').save()
        user.profile.secondname = "tester"
        user.profile.status = "0"
        user.profile.save()

    def test_user_is_added(self):
        """User is correctly added to db"""
        user = User.objects.get(username="Test")
        self.assertEqual(user.last_name, 'LTest')
        self.assertEqual(user.profile.secondname, 'tester')

class CourseTestCase(TestCase):
    def setUp(self):
        user = User.objects.create(username="Prof", first_name="Essor", last_name="Prof", email="test@test.test",
                                   password="0000")
        user.save()
        user.refresh_from_db()
        models.Profile(user_id=user.id,  identification_code='0', telegram_id='0').save()
        user.profile.secondname = "Prof"
        user.profile.status = "7"
        user.profile.save()
        user.refresh_from_db()
        if user.profile.status == "7":
            course = models.Course.objects.create(professor_id=user.id, template_id=1)
            course.name = "TestNM"
            course.audience = "Bachelors, 2nd year"
            course.save()

    def test_course_is_added(self):
        """User is correctly added to db"""
        course = models.Course.objects.get(name="TestNM")
        self.assertEqual(course.audience, 'Bachelors, 2nd year')


class TemplateTestCase(TestCase):
    def setUp(self):
            template = models.Template.objects.create()
            template.name = 'TestName'
            template.data = '{"CharFields":{"Your opinion about AI course?":""}, "IntFields":{"Total":{"min" :0,"val":0, "max":10 }}}'
            template.save()

    def test_template_is_added(self):
        """User is correctly added to db"""
        template = models.Template.objects.get(name="TestName")
        self.assertEqual(template.data, '{"CharFields":{"Your opinion about AI course?":""}, "IntFields":{"Total":{"min" :0,"val":0, "max":10 }}}')


class ScheduleTestCase(TestCase):
    def setUp(self):
        user = User.objects.create(username="Prof", first_name="Essor", last_name="Prof", email="test@test.test",
                                   password="0000")
        user.save()
        user.refresh_from_db()
        models.Profile(user_id=user.id, identification_code='0', telegram_id='0').save()
        user.profile.secondname = "Prof"
        user.profile.status = "7"
        user.profile.save()
        user.refresh_from_db()
        if user.profile.status == "7":
            course = models.Course.objects.create(professor_id=user.id, template_id=1)
            course.name = "TestNM"
            course.audience = "Bachelors, 2nd year"
            course.save()
        schedule = models.Schedule.objects.create(course_id=course.id)
        schedule.start = '23-42'
        schedule.end = '00-40'
        schedule.day = 'tuesday'
        schedule.save()

    def test_template_is_added(self):
        """User is correctly added to db"""
        schedule = models.Schedule.objects.get(start="23-42")
        self.assertEqual(schedule.day, "tuesday")
