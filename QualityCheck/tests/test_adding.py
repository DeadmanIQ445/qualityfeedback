from django.test import TestCase
from .. import models
from django.contrib.auth.models import User
from ..import views


class AddMemberCase(TestCase):
    def test_user_is_added(self):
        """User is correctly added to db"""
        views.add_member_to_db('test@email.te', 'test', 'test', 'testovich', 7)
        user = User.objects.get(username="test@email.te")
        self.assertEqual(user.last_name, 'test')


class AddCourseCase(TestCase):
    def setUp(self):
        views.add_member_to_db('test@email.te', 'test', 'test', 'testovich', 7)

    def test_user_is_added(self):
        """Course is correctly added to db"""
        views.add_course_to_db('test@email.te', 'test', 2)
        professor = User.objects.get(email='test@email.te')
        course = models.Course.objects.get(professor=professor)
        self.assertEqual(course.name, 'test')


class AddFormCase(TestCase):
    def setUp(self):
        views.add_member_to_db('test@email.te', 'test', 'test', 'testovich', 7)
        views.add_course_to_db('test@email.te', 'test', 2)
        views.add_member_to_db('test@stud.te', 'test', 'test', 'testovich', 2)
        student = User.objects.get(email='test@stud.te')
        student.profile.telegram_id = '1234'
        student.profile.save()

    def test_form_addition(self):
        """Answer is correctly added to db"""
        student = User.objects.get(email='test@stud.te')
        course = models.Course.objects.get(name='test')
        data = '{"CharFields": {"What do you think about Succi?": "He is great", "Else Blabla": "Blabla"}, "IntFields": {"overall": {"min": 0, "val": 9, "max": 10}}}'
        views.add_form_to_db(data=data, telegram_id=student.profile.telegram_id, course=course.id)
        ans = models.Form.objects.get(user=student)
        self.assertEqual(ans.data, data)


class AddTemplateCase(TestCase):
    def test_adding_template(self):
        """Template correctly added to db"""
        name = 'test'
        data = '{"CharFields": {"What do you think about Succi?": "He is great", "Else Blabla": "Blabla"}, "IntFields": {"overall": {"min": 0, "val": 9, "max": 10}}}'
        views.add_template_to_db(name,  data)
        template = models.Template.objects.get(name=name)
        self.assertEqual(data, template.data)