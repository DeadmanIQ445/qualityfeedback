from django.test import TestCase
from .. import models
from django.contrib.auth.models import User
from ..import views


class AssignTemplateCase(TestCase):
    def setUp(self):
        name = 'test'
        data = '{"CharFields": {"What do you think about Succi?": "He is great", "Else Blabla": "Blabla"}, "IntFields": {"overall": {"min": 0, "val": 9, "max": 10}}}'
        views.add_template_to_db(name, data)
        views.add_member_to_db('test@email.te', 'test', 'test', 'testovich', 7)
        views.add_course_to_db('test@email.te', 'test', 2)

    def test_assign_template(self):
        """Template is correctly assigned to course"""
        course = models.Course.objects.get(name='test')
        template = models.Template.objects.get(name='test')
        views.assign_template_db(course.id, template.id)
        course.refresh_from_db()
        self.assertEqual(course.template_id, template.id)
