from django import forms


class ProfileForm(forms.Form):
    email = forms.CharField(label="email", max_length=50)
    first_name = forms.CharField(label="first_name", max_length=50)
    second_name = forms.CharField(label="second_name", max_length=50)
    last_name = forms.CharField(label="last_name", max_length=50)
    status = forms.CharField(label="status", max_length=50)


class CourseForm(forms.Form):
    name = forms.CharField(label="name", max_length=50)
    audience = forms.CharField(label="audience", max_length=50)
    professor = forms.IntegerField(label="professor")


class TemplateForm(forms.Form):
    name = forms.CharField(label='name', max_length=50)
    data = forms.CharField(label='data', max_length=1024)


class FormForm(forms.Form):
    data = forms.CharField(label='data', max_length=2000)


class SendForm(forms.Form):
    course = forms.IntegerField(label='course')


class GetAnswersForm(forms.Form):
    user = forms.IntegerField(label='user')


class AssignTemplatForm(forms.Form):
    course = forms.IntegerField(label='course')
    template = forms.IntegerField(label='template')

class PieForm(forms.Form):
    course = forms.IntegerField(label='course')
    type = forms.CharField(label='type', max_length=10)
    number = forms.IntegerField(label='number')
    
class StatForm(forms.Form):
    course = forms.IntegerField(label='course')
    type = forms.CharField(label='template', max_length=10)

class PassForm(forms.Form):
    password = forms.CharField(label='password', max_length=16)