import requests
from bot_app import objects



class ApiClient:
    '''
    Client for communicating with server API
    '''

    def __init__(self):
        self.base_url = 'http://127.0.0.1:8000/'


    def _send(self, method, endpoint, data):
        '''
        Send get or post request to specified endpoint with data
        :param method: GET or POST request
        :param endpoint: endpoint for specific method
        :param data: dict object to pass as params or data
        :return: request.Response object
        '''
        if method.lower() == "get":
            return requests.get(self.base_url + endpoint + '/', data)
        elif method.lower() == 'post':
            return requests.post(self.base_url + endpoint + '/', data)

    def bind_tg_id(self, telegram_id, identification_code):
        '''
        Bind telegram user id with obtained verification code
        :param telegram_id: telegram user id
        :param identification_code: sent by user verification code
        :return: json response
        '''
        data = {"identification_code": identification_code, "tg_id": str(telegram_id)}
        response = self._send("POST", 'bind', data)
        if response.ok:
            return response.json()
        else:
            # TODO: add logger
            pass

    def get_audience_for_course(self, course_id):
        data = {'course': course_id}
        response = self._send('POST', 'get_audience', data)
        if response.ok:
            js = response.json()
            user_ids = [v.get('telegram_id') for v in js.values()]
            return user_ids
        return []

    def get_schedule_and_templates(self):
        '''
        Get schedule of polls and their templates
        Asks API service to get the information

        Warning: Assigns the same template for different types of course classes
        :return: list of CourseClass objects
        '''
        # TODO: separate types of classes or they have the same template?
        response = self._send('GET', 'schedule', {})

        timetable = response.json()

        course_objects = []
        if isinstance(timetable, dict):
            for course_id, info in timetable.items():
                response = self._send(method='POST', endpoint='template_by_course', data={'course': course_id})
                template = objects.CourseTemplate()
                template.init_template(response.json())

                inf = info.get('data')
                course_name = info.get('name')
                for d in inf:
                    cl = objects.CourseClass(course_id)
                    cl.course_name = course_name
                    cl.template = template
                    cl.init_schedule(d)
                    course_objects.append(cl)
        return course_objects

    def commit_feedback_from_user(self, telegram_id, course_id, feedback):
        '''
        Commit and save feedback left from user for a particular poll event

        Warning: does not separates the feedback from different types of classes
        :param telegram_id: telegram id of the user
        :param course_id: id of the course
        :param feedback: dict of user's answers for poll template
        :return:
        '''
        # TODO: separate feedback from different class types for the course
        data = {
            'course': course_id,
            'telegram_id': telegram_id,
            'data': str(feedback).replace('\'', '\"') # accepts not json but string of json
        }

        response = self._send("POST", 'add_form', data)
        if response.ok:
            data = response.json()
            return data
        # TODO: add logger
        pass
