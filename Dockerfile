FROM python:3.7-slim

# Set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Set work directory
WORKDIR /code
COPY . /code/
# Install dependencies
COPY manage.py requirements.txt /code/
RUN pip install -r requirements.txt
RUN pip install psycopg2-binary




CMD ["python", "manage.py", "runserver", "0.0.0.0:8001"]